var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var configDB = require('../../config/database');
var bloodDonorService = require('../../service/bloodDonor/');
var url = configDB.url;

var createEntity = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.createEntity(db, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getAllEntity = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.findAllEntity(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getSingleEntity = (req, res) => {

    var ObjectID = require('mongodb').ObjectID;
    var o_id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.findSingleEntity(db, o_id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updateEntity = (req, res) => {
    console.log('In controller update section doc');

    console.log(req.body);

    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.updateEntity(db, id, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var deleteEntity = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.deleteEntity(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    createEntity,
    getAllEntity,
    getSingleEntity,
    updateEntity,
    deleteEntity
};