var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var configDB = require('../../config/database');
var userService = require('../../service/user');
var url = configDB.url;

var testControllerMethod = (req, res) => {
    res.status(200).json({"message": "Hello World from user controller!!!"});
};

var createUser = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.createUser(db, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getAllUser = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.findAllUser(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getSingleUser = (req, res) => {

    var ObjectID = require('mongodb').ObjectID;
    var o_id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.findSingleUser(db, o_id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updateUser = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.updateUser(db, id, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var deleteUser = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        userService.deleteUser(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    testControllerMethod,
    createUser,
    getAllUser,
    getSingleUser,
    updateUser,
    deleteUser
};