var jwt = require('jsonwebtoken');
var User = require('../../models/user/');
var config = require('../../config/database');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var configDB = require('../../config/database');
var bloodDonorService = require('../../service/bloodDonor/');
var url = configDB.url;

var authenticateUser = (req, res) => {
    console.log('Email: ' + req.body.bloodDonorEmail);
    
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodDonorService.findAllEntity(db, (users) => {
            db.close();
            let index = 0;

            for(let user of users) {
                index = index + 1;

                console.log(user.bloodDonorEmail);

                if (user.bloodDonorEmail == req.body.bloodDonorEmail) {
                    if (user.bloodDonorPassword == req.body.bloodDonorPassword) {
                        var token = jwt.sign(user, config.mySecret, {
                            expiresIn: 1440  //24 hours
                        });
                        return res.status(200).json({
                            success: true,
                            message: 'Token Created !!!',
                            token: token,
                            id: user._id
                        });
                    } else {
                        return res.status(500).json({
                            success: false,
                            message: 'Authentication failed: Password dose not match'
                        });
                    }
                }
                if(index == users.length) {
                    return res.status(500).json({
                        success: false,
                        message: 'Authentication failed: User not Found'
                    });
                }
            }
        });
    });
};

var checkAuthentication = (req, res, next) => {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token) {
        jwt.verify(token, config.mySecret, (err, decoded) => {
            if(err) {
                return res.status(404).json({
                    success: false,
                    message: 'Failed to authenticate token'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No Token Provided'
        });
    }
};

module.exports = {
    authenticateUser,
    checkAuthentication
};