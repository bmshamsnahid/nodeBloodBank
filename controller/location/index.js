var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var configDB = require('../../config/database');
var locationService = require('../../service/location/');
var url = configDB.url;

var createEntity = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        locationService.createEntity(db, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getAllEntity = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        locationService.findAllEntity(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var getSingleEntity = (req, res) => {

    var ObjectID = require('mongodb').ObjectID;
    var o_id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        locationService.findSingleEntity(db, o_id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updateEntity = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        locationService.updateEntity(db, id, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var deleteEntity = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        locationService.deleteEntity(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    createEntity,
    getAllEntity,
    getSingleEntity,
    updateEntity,
    deleteEntity
};