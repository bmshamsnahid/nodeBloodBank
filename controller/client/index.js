var configDB = require('../../config/database');
var url = configDB.url;
var mongoose = require('mongoose');
var clientModel = require('../../mongo/model/client');

var createClient = (req, res) => {
    var client = new clientModel({
		clientId: keyGen(10),
		clientSecret: keyGen(20)
    });
    
    client.save(function(err, client) {
        if (err) {
            res.status(404).json(err);
        } else {
            res.status(200).json(client);
        }
    });
};

module.exports = {
    createClient
};

function keyGen(keyLength) {
    var i, key = "", characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    var charactersLength = characters.length;

    for (i = 0; i < keyLength; i++) {
        key += characters.substr(Math.floor((Math.random() * charactersLength) + 1), 1);
    }

    return key;
}