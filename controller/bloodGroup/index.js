var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var configDB = require('../../config/database');
var bloodGroupService = require('../../service/bloodGroup');
var url = configDB.url;

var createBloodGroup = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodGroupService.createBloodGroup(db, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var findAllBloodGroup = (req, res) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodGroupService.findAllBloodGroup(db, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var findSingleBloodGroup = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var o_id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodGroupService.findSingleBloodGroup(db, o_id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var updateBloodGroup = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodGroupService.updateBloodGroup(db, id, req.body, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

var deleteBloodGroup = (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = new ObjectID(req.params.id);

    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        bloodGroupService.deleteBloodGroup(db, id, (docs) => {
            db.close();
            res.status(200).json(docs);
        });
    });
};

module.exports = {
    createBloodGroup,
    findAllBloodGroup,
    findSingleBloodGroup,
    updateBloodGroup,
    deleteBloodGroup
};