// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var path     = require('path');
var port     = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var cors = require('cors');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

var authRoutes   = require('./routes/auth');
var userRoutes   = require('./routes/user');
var viewRoutes   = require('./routes/viewRoutes');
var bloodGroupRoutes = require('./routes/bloodGroup');
var bloodDonorRoutes = require('./routes/bloodDonor');
var locationRoutes = require('./routes/location');

var configDB     = require('./config/database');

app.use(cors());

var authenticateController = require('./controller/auth/');

//for angular static files =====================================================
app.use(express.static(path.join(__dirname, 'public')));

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

//Enable cors supports ==========================================================
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
//   next();
// });

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({
    secret: 'ilovescotchscotchyscotchscotch', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
app.use('/auth', authRoutes);
app.use('/user', authenticateController.checkAuthentication, userRoutes);
app.use('/view', viewRoutes);
app.use('/bloodGroup', authenticateController.checkAuthentication, bloodGroupRoutes);
app.use('/bloodDonor', bloodDonorRoutes);
app.use('/location', authenticateController.checkAuthentication, locationRoutes);

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);