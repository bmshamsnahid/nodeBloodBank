var express = require('express');
var router = express.Router();

var clientController = require('../../controller/client');

router.get('/', clientController.createClient);

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.user) {
        return next();
    } else {
        res.status(200).json({
            'message': 'Authentication failed'
        });
    }
}