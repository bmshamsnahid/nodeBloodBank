var express = require('express');
var router = express.Router();

var locationController = require('../../controller/location');

router.post('/', locationController.createEntity);
router.get('/', locationController.getAllEntity);
router.get('/:id', locationController.getSingleEntity);
router.put('/:id', locationController.updateEntity);
router.delete('/:id', locationController.deleteEntity);

module.exports = router;