var express = require('express');
var router = express.Router();
var bloodGroupController = require('../../controller/bloodGroup');

router.post('/', bloodGroupController.createBloodGroup);
router.get('/', bloodGroupController.findAllBloodGroup);
router.get('/:id', bloodGroupController.findSingleBloodGroup);
router.put('/:id', bloodGroupController.updateBloodGroup);
router.delete('/:id', bloodGroupController.deleteBloodGroup);

module.exports  = router;