var express = require('express');
var router = express.Router();
var authenticateController = require('../../controller/auth/');

var userController = require('../../controller/user');

router.post('/', userController.createUser);
router.get('/', authenticateController.checkAuthentication, userController.getAllUser);
router.get('/:id', authenticateController.checkAuthentication, userController.getSingleUser);
router.put('/:id', authenticateController.checkAuthentication, userController.updateUser);
router.delete('/:id', authenticateController.checkAuthentication, userController.deleteUser);

module.exports = router;