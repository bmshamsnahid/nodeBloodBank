var express = require('express');
var passport = require('passport');
var authController = require('../../controller/auth');
var router = express.Router();
var authenticateController = require('../../controller/auth/');

/*router.get('/', authController.testControllerMethod);

//local auth using username and password
router.post('/signup', authController.userSignUp);
router.post('/login', authController.userLogin);
router.get('/profile', isLoggedIn, authController.getUserProfile);
router.get('/logout', authController.userLogout);*/

router.post('/authenticate', authenticateController.authenticateUser);
//router.post('/user', userController.createUser);
//router.get('/users', authenticateController.checkAuthentication, userController.getUsers);

module.exports = router;

//route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}