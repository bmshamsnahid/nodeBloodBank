var express = require('express');
var router = express.Router();

var bloodDonorController = require('../../controller/bloodDonor');
var authenticateController = require('../../controller/auth/');

router.post('/', bloodDonorController.createEntity);
router.get('/', authenticateController.checkAuthentication, bloodDonorController.getAllEntity);
router.get('/:id', authenticateController.checkAuthentication, bloodDonorController.getSingleEntity);
router.put('/:id', authenticateController.checkAuthentication, bloodDonorController.updateEntity);
router.delete('/:id', authenticateController.checkAuthentication, bloodDonorController.deleteEntity);

module.exports = router;