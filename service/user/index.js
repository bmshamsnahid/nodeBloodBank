var assert = require('assert');

var createUser = (db, docs, callback) => {
    var collection = db.collection('users');
    collection.insertOne(docs, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

var findAllUser = function(db, callback) {
    var collection = db.collection('users');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
}

var findSingleUser = function(db, id, callback) {
    var collection = db.collection('users');

    collection.find({"_id": id}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var  updateUser = (db, id, docs, callback) => {
    var collection = db.collection('users');
    collection.updateOne({"_id": id}, { $set: docs }, function(err, result) {
            assert.equal(err, null);
            callback(result);
    });
};

var deleteUser = (db, id, callback) => {
    var collection = db.collection('users');
    collection.deleteOne({"_id": id}, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

module.exports = {
    createUser,
    findAllUser,
    findSingleUser,
    updateUser,
    deleteUser
};