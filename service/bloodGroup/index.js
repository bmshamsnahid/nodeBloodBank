var assert = require('assert');

var createBloodGroup = (db, docs, callback) => {
    var collection = db.collection('blood-group');
    collection.insertOne(docs, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

var findAllBloodGroup = (db, callback) => {
    var collection = db.collection('blood-group');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var findSingleBloodGroup = (db, id, callback) => {
    var collection = db.collection('blood-group');

    collection.find({"_id": id}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var updateBloodGroup = (db, id, docs, callback) => {
    var collection = db.collection('blood-group');
    collection.updateOne({"_id": id}, { $set: docs }, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

var deleteBloodGroup = (db, id, callback) => {
    var collection = db.collection('blood-group');
    collection.deleteOne({"_id": id}, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

module.exports = {
    createBloodGroup,
    findAllBloodGroup,
    findSingleBloodGroup,
    updateBloodGroup,
    deleteBloodGroup
};