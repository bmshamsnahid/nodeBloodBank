var assert = require('assert');

var createEntity = (db, docs, callback) => {
    var collection = db.collection('location');
    collection.insertOne(docs, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

var findAllEntity = function(db, callback) {
    var collection = db.collection('location');
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
}

var findSingleEntity = function(db, id, callback) {
    var collection = db.collection('location');

    collection.find({"_id": id}).toArray(function(err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
};

var  updateEntity = (db, id, docs, callback) => {
    var collection = db.collection('location');
    collection.updateOne({"_id": id}, { $set: docs }, function(err, result) {
            assert.equal(err, null);
            callback(result);
    });
};

var deleteEntity = (db, id, callback) => {
    var collection = db.collection('location');
    collection.deleteOne({"_id": id}, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
};

module.exports = {
    createEntity,
    findAllEntity,
    findSingleEntity,
    updateEntity,
    deleteEntity
};